﻿#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/photo/photo.hpp>
#include <iostream>
#include <vector>
#include <limits>
#include <queue>

#define M_PI          3.141592653589793238462643383279502884L

using namespace cv;
using namespace std;

Mat matched;
Mat src;
Mat image;
Mat sample;
Mat	gray;
Mat edges;
// find contours in whole image
std::vector<std::vector<cv::Point> > contours, filteredContours;
std::vector<cv::Rect> boundingBoxes;
vector<cv::Rect> alignedBoundingBoxes, tmpRes;

int getDigitMinHeight()
{
	return 25;
}

int getDigitMaxHeight()
{
	return 50;
}

int getDigitYAlignment()
{
	return 20;
}

class sortRectByX {
public:
	bool operator()(cv::Rect const & a, cv::Rect const & b) const {
		return a.x < b.x;
	}
};

class sortRectByX_v3f {
public:
	bool operator()(cv::Vec3f const & a, cv::Vec3f const & b) const {
		return a[0] < b[0];
	}
};

class sortRectByX_rr {
public:
	bool operator()(cv::RotatedRect const & a, cv::RotatedRect const & b) const {
		return a.center.x < b.center.x;
	}
};

void filterContours(std::vector<std::vector<cv::Point> >& contours,
	std::vector<cv::Rect>& boundingBoxes, std::vector<std::vector<cv::Point> >& filteredContours) 
{
	// filter contours by bounding rect size
	for (size_t i = 0; i < contours.size(); i++) 
	{
		cv::Rect bounds = cv::boundingRect(contours[i]);
		if (bounds.height > getDigitMinHeight() && bounds.height < getDigitMaxHeight()
			&& bounds.width > 5 && bounds.width < bounds.height) 
		{
			boundingBoxes.push_back(bounds);
			filteredContours.push_back(contours[i]);
		}
	}
}

void findAlignedBoxes(std::vector<cv::Rect>::const_iterator begin,
	std::vector<cv::Rect>::const_iterator end, std::vector<cv::Rect>& result) 
{
	std::vector<cv::Rect>::const_iterator it = begin;
	cv::Rect start = *it;
	++it;
	result.push_back(start);

	for (; it != end; ++it) {
		if (abs(start.y - it->y) < getDigitYAlignment() && abs(start.height - it->height) < 5) {
			result.push_back(*it);
		}
	}
}



cv::Mat _samples;
cv::Mat _responses;
CvKNearest* _pModel;

/**
* Prepare an image of a digit to work as a sample for the model.
*/
cv::Mat prepareSample(const cv::Mat& img) 
{
	cv::Mat roi, sample;
	cv::resize(img, roi, cv::Size(20, 20));
	roi.reshape(1, 1).convertTo(sample, CV_32F);
	return sample;
}

/**
* Learn a single digit.
*/
int learn(const cv::Mat & img) 
{
	cv::imshow("Learn", img);
	int key = cv::waitKey(0);
	if (key >= '0' && key <= '9') {
		_responses.push_back(cv::Mat(1, 1, CV_32F, (float)key - '0'));
		_samples.push_back(prepareSample(img));
	}

	return key;
}

/**
* Initialize the model.
*/
void initModel() 
{
	_pModel = new CvKNearest(_samples, _responses);
}

int getOcrMaxDist()
{
	return 10;
}

/**
* Recognize a single digit.
*/
char recognize(const cv::Mat& img) 
{
	char cres = '?';
	cv::Mat results, neighborResponses, dists;
	float result = _pModel->find_nearest(
		prepareSample(img), 2, results, neighborResponses, dists);

	int ocdDist = dists.at<float>(0, 0);
	int a = neighborResponses.at<float>(0, 0);
	int b = neighborResponses.at<float>(0, 1);
	if (0 == int(a - b) && ocdDist < getOcrMaxDist())
	{
		cres = '0' + (int)result;
	}
	return cres;
}

char* getTrainingDataFilename()
{
	return "ocr.learn";
}

/**
* Save training data to file.
*/
void saveTrainingData() 
{
	cv::FileStorage fs(getTrainingDataFilename(), cv::FileStorage::WRITE);
	fs << "samples" << _samples;
	fs << "responses" << _responses;
	fs.release();
}

/**
* Load training data from file and init model.
*/
bool loadTrainingData() 
{
	cv::FileStorage fs(getTrainingDataFilename(), cv::FileStorage::READ);
	if (fs.isOpened()) {
		fs["samples"] >> _samples;
		fs["responses"] >> _responses;
		fs.release();

		initModel();
	}
	else {
		return false;
	}
	return true;
}

void maxLocs(const Mat& src, queue<Point>& dst, size_t size)
{
	float maxValue = -1.0f * numeric_limits<float>::max();
	float* srcData = reinterpret_cast<float*>(src.data);

	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			if (srcData[i*src.cols + j] > maxValue)
			{
				maxValue = srcData[i*src.cols + j];

				dst.push(Point(j, i));

				// pop the smaller one off the end if we reach the size threshold.
				if (dst.size() > size)
				{
					dst.pop();
				}
			}
		}
	}
}

float euclideanDist(Point& p, Point& q) 
{
	Point diff = p - q;
	return cv::sqrt(diff.x * diff.x + diff.y * diff.y);
}

int main(int argc, char** argv)
{

	VideoCapture cap;
	// open the default camera, use something different from 0 otherwise;
	// Check VideoCapture documentation.
	/*if (!cap.open(0))
		return 0;
	for (;;)
	{
		Mat frame;
		cap >> frame;
		if (frame.empty()) break; // end of video stream
		imshow("this is you, smile! :)", frame);
		if (waitKey(1) == 27) break; // stop capturing by pressing ESC 
	}
	// the camera will be closed automatically upon exit
	// cap.close();
	return 0;*/

	if (argc != 2)
	{
		cout << " Usage: display_image ImageToLoadAndDisplay" << endl;
		return -1;
	}

	src = imread("x64\\Debug\\_img.jpg", IMREAD_COLOR); // Read the file

	if (!src.data) // Check for invalid input
	{
		cout << "Could not open or find the image" << std::endl;
		return -1;
	}

	/// Compute a rotation matrix with respect to the center of the image
	Point center = Point(src.cols / 2, src.rows / 2);
	double angle = -180.0;
	double scales = 1;

	Mat srcs;

	Mat rot_mat(2, 3, CV_32FC1);
	/// Get the rotation matrix with the specifications above
	rot_mat = getRotationMatrix2D(center, angle, scales);
	/// Rotate the warped image
	warpAffine(src, srcs, rot_mat, src.size());

	//cv::GaussianBlur(sm, srcs, cv::Size(0, 0), 3);
	//cv::addWeighted(src, 1.5, srcs, -0.5, 0, srcs);

	Mat new_image;

	double alpha = 1.1;
	int beta = 10;
	new_image = Mat::zeros(srcs.size(), srcs.type());

	for (int y = 0; y < srcs.rows; y++)
	{
		for (int x = 0; x < srcs.cols; x++)
		{
			for (int c = 0; c < 3; c++)
			{
				new_image.at<Vec3b>(y, x)[c] =
					saturate_cast<uchar>(alpha*(srcs.at<Vec3b>(y, x)[c]) + beta);
			}
		}
	}
	
	Mat sm;
	//smooth the image using Gaussian kernel in the "src" and save it to "dst"
	GaussianBlur(new_image, sm, Size(9, 9), 0, 0);

	Mat3b hsv;
	cvtColor(sm, hsv, COLOR_BGR2HSV);

	Mat3b hls;
	cvtColor(sm, hls, COLOR_BGR2HLS);

	Mat1b wht_mask1, wht_mask2;
	// for yelloww 
	inRange(hsv, Scalar(10, 0, 0), Scalar(90, 255, 255), wht_mask1);

	Mat1b blk_mask1;// , blk_mask1;
	inRange(hls, Scalar(0, 0, 0), Scalar(255, 40, 255), blk_mask1);

	Mat1b circle_mask = wht_mask1 | blk_mask1;

	Mat1b gray;
	inRange(hls, Scalar(0, 50, 0), Scalar(150, 200, 50), gray);

	int radius_def = 45;
	vector<Vec3f> circles_40_50;
	Canny(circle_mask, edges, 200, 400);
	HoughCircles(edges, circles_40_50, CV_HOUGH_GRADIENT, 1, 15, 5, 15, 40, 50);
	sort(circles_40_50.begin(), circles_40_50.end(), sortRectByX_v3f());
	/// Draw the circles detected
	for (size_t i = 0; i < circles_40_50.size(); i++)
	{
		if (i == 0)
		{
			circles_40_50[i][0] += 5;
		}
		if (i == 2)
		{
			circles_40_50[i][0] -= 5;
			circles_40_50[i][1] -= 5;
		}
		if (i == 3)
		{
			circles_40_50[i][0] -= 5;			
		}

		Point center(cvRound(circles_40_50[i][0]), cvRound(circles_40_50[i][1]));
		int radius = cvRound(circles_40_50[i][2]);
		// circle center
		circle(sm, center, 2, Scalar(255, 0, 0), 2, 8, 0);
		// circle outline
		circle(sm, center, radius_def, Scalar(255, 0, 0), 2, 8, 0);
	}


	cv::Mat maskedWhite; // stores masked Image
	cv::Mat maskedBlack; // stores masked Image
	cv::Mat mask_wht(sm.size(), sm.type());  // create an Mat that has same Dimensons as src
	cv::Mat mask_blk(sm.size(), sm.type());  // create an Mat that has same Dimensons as src
	mask_wht.setTo(cv::Scalar(0, 0, 0));                                            // creates black-Image
	mask_blk.setTo(cv::Scalar(0, 0, 0));                                            // creates black-Image
																				// Add all found circles to mask
	for (size_t i = 0; i < circles_40_50.size(); i++)                          // iterate through all detected Circles
	{
		cv::Point center(cvRound(circles_40_50[i][0]), cvRound(circles_40_50[i][1])); // CVRound converts floating numbers to integer
	
		if (i == 1)
			cv::circle(mask_blk, center, radius_def, cv::Scalar(255, 255, 255), -1, 8, 0);
		else
			cv::circle(mask_wht, center, radius_def, cv::Scalar(255, 255, 255), -1, 8, 0);
	}

	sm.copyTo(maskedWhite, mask_wht); // creates masked Image and copies it to maskedImage
	sm.copyTo(maskedBlack, mask_blk); // creates masked Image and copies it to maskedImage

	
	Point2f centerv;
	float radius;
	vector<Vec2f> lines;

	cvtColor(maskedWhite, hsv, COLOR_BGR2HSV);

	Mat1b red_mask1, red_mask2;
	inRange(hsv, Scalar(0, 30, 30), Scalar(10, 255, 255), red_mask1);
	inRange(hsv, Scalar(160, 30, 30), Scalar(179, 255, 255), red_mask2);
	Mat1b red_mask = red_mask1 | red_mask2;

	Canny(red_mask, edges, 200, 400);
	cv::findContours(edges, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

	vector<RotatedRect> ellipses;
	for (int i = 0; i < contours.size(); ++i)
	{		
		if (contours[i].size() > 30)
		{
			RotatedRect elps = fitEllipse(Mat(contours[i]));					
			if (elps.size.height > 25 && elps.size.width > 10)
			{
				ellipses.push_back(elps);
			}
		}
	}
	
	sort(ellipses.begin(), ellipses.end(), sortRectByX_rr());

	int elpsNo = 0;
	for (int i = 0; i < 4; ++i)
	{
		if (i == 1)
			continue;
		Vec3f circ = circles_40_50[i];
		RotatedRect elps = ellipses[elpsNo++];

		//ellipse(sm, elps, Scalar(0, 200, 0), 1, 8);
		//circle(sm, elps.center, 2, Scalar(0, 200, 0), 1, 8, 0);
		
		Point e1;
		e1.x = (elps.size.height / 2) * cos((elps.angle + 90) * M_PI / 180) + elps.center.x;
		e1.y = (elps.size.height / 2) * sin((elps.angle + 90) * M_PI / 180) + elps.center.y;

		Point e2;
		e2.x = (elps.size.height / 2) * cos((elps.angle - 90) * M_PI / 180) + elps.center.x;
		e2.y = (elps.size.height / 2) * sin((elps.angle - 90) * M_PI / 180) + elps.center.y;

		float d1 = euclideanDist(Point(circ[0], circ[1]), e1);
		float d2 = euclideanDist(Point(circ[0], circ[1]), e2);
		Point pClosestToCenter = d1 < d2 ? e1 : e2;
		Point pLongestToCenter = d1 < d2 ? e2 : e1;

		circle(sm, pClosestToCenter, 2, Scalar(0, 0, 200), 2, 8, 0);
		line(sm, e1, e2, Scalar(0, 200, 0), 2);

		float dX = pLongestToCenter.x - pClosestToCenter.x;
		float dY = pLongestToCenter.y - pClosestToCenter.y;

		float angle = atan2(dY, dX) * 180 / M_PI + 90;
		if (angle < 0)
			angle += 360;

		Point t = elps.center;
		t.y += 50;
		char buff[50];
		putText(sm, itoa(angle, buff, 10), t, FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0, 255, 0), 2.0);
	}


	Mat1b wht_mask;
	cvtColor(maskedBlack, hls, COLOR_BGR2HLS);	
	inRange(hls, Scalar(0, 50, 0), Scalar(255, 100, 255), wht_mask);

	Canny(wht_mask, edges, 200, 400);
	cv::findContours(edges, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
	for (int i = 0; i < contours.size(); ++i)
	{
		if (contours[i].size() > 5)
		{
			RotatedRect elps = fitEllipse(Mat(contours[i]));
			if (elps.size.height > 4 && elps.size.width > 4
				&& (elps.size.height / elps.size.width) > 0.5
				&& (elps.size.height / elps.size.width) < 2)
			{
				Vec3f mcirc = circles_40_50[1];
				Point mcP = Point(mcirc[0], mcirc[1]);
				
				line(sm, mcP, elps.center, Scalar(0, 200, 0), 2);
				circle(sm, mcP, 2, Scalar(0, 0, 200), 2, 8, 0);

				float dX = elps.center.x - mcirc[0];
				float dY = elps.center.y - mcirc[1];

				float angle = atan2(dY, dX) * 180 / M_PI + 90;
				if (angle < 0)
					angle += 360;
								
				mcP.y += 50;
				char buff[50];
				putText(sm, itoa(angle, buff, 10), mcP, FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0, 255, 0), 2.0);
			}
		}
	}
	
	/*
	alpha = 2.5;
	beta = 100;
	for (int y = 0; y < sm.rows; y++)
	{
		for (int x = 0; x < sm.cols; x++)
		{
			for (int c = 0; c < 3; c++)
			{
				sm.at<Vec3b>(y, x)[c] =
					saturate_cast<uchar>(alpha*(sm.at<Vec3b>(y, x)[c]) + beta);
			}
		}
	}*/

	sm.convertTo(sm, -1, 1.25, 0); //increase the contrast (double)

	cvtColor(sm, gray, COLOR_BGR2GRAY);

	imshow("Display window", gray); // Show our image inside it.
	waitKey(0); // Wait for a keystroke in the window

	//cvtColor(sm, gray, COLOR_BGR2HSV);
	//Mat1b blk_mask1;// , blk_mask1;
	inRange(hsv, Scalar(0, 0, 30, 0), Scalar(180, 255, 165, 0), blk_mask1);
	imshow("Display window", blk_mask1); // Show our image inside it.
	waitKey(0); // Wait for a keystroke in the window

	// ???? ??????? ?????????? Canny
	Canny(gray, edges, 75, 100);
	namedWindow("Display window", WINDOW_AUTOSIZE); // Create a window for display.

	imshow("Display window", edges); // Show our image inside it.
	waitKey(0); // Wait for a keystroke in the window

	cv::Mat img_ret = edges.clone();
		
	/**/
	cv::findContours(edges, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

	// filter contours by bounding rect size
	filterContours(contours, boundingBoxes, filteredContours);

	// find bounding boxes that are aligned at y position
	for (vector<cv::Rect>::const_iterator ib = boundingBoxes.begin(); ib != boundingBoxes.end(); ++ib)
	{
		tmpRes.clear();
		findAlignedBoxes(ib, boundingBoxes.end(), tmpRes);
		if (tmpRes.size() > alignedBoundingBoxes.size()) {
			alignedBoundingBoxes = tmpRes;
		}
	}

	// sort bounding boxes from left to right	
	sort(alignedBoundingBoxes.begin(), alignedBoundingBoxes.end(), sortRectByX());

	//cv::Mat img_ret = edges.clone();

	std::vector<cv::Mat> _digits;
	// cut out found rectangles from edged image
	for (int i = 0; i < alignedBoundingBoxes.size(); ++i) 
	{
		cv::Rect roi = alignedBoundingBoxes[i];
		_digits.push_back(img_ret(roi) );
		//if (_debugDigits) {
			cv::rectangle(sm, roi, cv::Scalar(255, 0, 0), 1);
		//}
	}
	
	namedWindow("Display window", WINDOW_AUTOSIZE); // Create a window for display.
	imshow("Display window", sm); // Show our image inside it.
	waitKey(0); // Wait for a keystroke in the window

	for (vector<cv::Mat>::iterator it = _digits.begin(); it != _digits.end(); ++it)
	{
		learn(*it);
	}
	saveTrainingData();
	waitKey(0); // Wait for a keystroke in the window

	initModel();
	for (vector<cv::Mat>::iterator it = _digits.begin(); it != _digits.end(); ++it)
		char result = recognize(*it);
	
	
	waitKey(0); // Wait for a keystroke in the window
	return 0;
}

