#include <string>
#include <algorithm>
#include <sstream>


const byte STX = 0x02;
const byte ETX = 0x03;

const byte CR = 0x0A;
const byte LF = 0x0D;

const char* GET_DEVICE_DATA = "/?!";

#define REQ_1_4_LEN		(7 * 10)
#define REQ_2_3_LEN		(11 + 7 * 9)

typedef struct
{
	string manuf;
	string model;
	float values[4];
} en61107_dev_response_t;

typedef struct
{
	float values[10];
} en61107_response_t;

using namespace std;

int en61107_decode_dev_data(char *frame, unsigned char frame_length, en61107_dev_response_t *response)
{
	string line;
	stringstream ss(frame);

	getline(ss, line, '\n');
	response->manuf = line.substr(1, 3);
	response->model = line.substr(5);

	if (frame != NULL)
	{
		int i;
		for (i = 0; i < 4 && getline(ss, line, '\n'); ++i)
		{
			if (line[0] == STX)			
				line = line.substr(1);
			if (line[0] == ETX)
				break;
			string val = line.substr(0, line.find('(') - 1);
			response->values[i] = atof(val.c_str());
		}
	}
}

int en61107_decode_frame(char *frame, unsigned char frame_length, en61107_response_t *response)
{
	string reg;
	stringstream line(frame);
	if (frame != NULL)
	{
		int i;
		for (i = 0; i < 10 && std::getline(line, reg, ' '); ++i)
		{
			response->values[i++] = atof(reg.c_str());
		}
		return i;
	}
	return 0;
}