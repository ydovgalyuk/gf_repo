#pragma once
#ifndef COMMAND_H
#define COMMANDS_H

#define K602_VOLUME    0x0044
#define K602_POWER    0x0050
#define K602_FORWARD_TEMP  0x0056
#define K602_RETURN_TEMP  0x0057
#define K602_DIFF_TEMP   0x0059
#define K602_ENERGY    0x003C
#define K602_TIMESTAMP   0x03EA

extern  uint16_t    K602commands[];
//#define K602_COUNT  (sizeof(K602commands)/sizeof(uint16_t))
#define K602_COUNT  7

#define K382_ENERGY_IN   0x0001
#define K382_ENERGY_OUT       0x0002
#define K382_ENERGY_IN_HI_RES  0x000D
#define K382_ENERGY_OUT_HI_RES 0x000E
#define K382_VOLTAGE_P1   0x041E
#define K382_VOLTAGE_P2   0x041F
#define K382_VOLTAGE_P3   0x0420
#define K382_CURRENT_P1   0x0434
#define K382_CURRENT_P2   0x0435
#define K382_CURRENT_P3   0x0436
#define K382_POWER_P1   0x0438
#define K382_POWER_P2   0x0439
#define K382_POWER_P3   0x043A
#define K382_TIMESTAMP   0x03EA

extern  uint16_t     K382commands[];
//#define K382_COUNT  sizeof(K382commands)/sizeof(uint16_t)
#define K382_COUNT   14

#if K602_COUNT > K382_COUNT
#define _COUNT K602_COUNT
#else
#define _COUNT K382_COUNT
#endif

/*
// KAMSTRUP MULTICAL 602 COMMANDS
#define K602_CMD_MAX_ COUNT 71
const cmdtxt_t _cmdtxt[ K602_CMD_COUNT] =
{
{0x03EB,"Aktuel dato (YYMMDD)"},
{0x003C,"Energiregister 1: Varmeenergi"},
{0x005E,"Energiregister 2: Kontrolenergi"},
{0x003F,"Energiregister 3: K�leenergi"},
{0x003D,"Energiregister 4: Freml�bsenergi"},
{0x003E,"Energiregister 5: Returl�bsenergi"},
{0x005F,"Energiregister 6: Tappevandsenergi"},
{0x0060,"Energiregister 7: Varmenergi Y"},
{0x0061,"Energiregister 8: ?m3 x T1?"},
{0x006E,"Energiregister 9: ?m3 x T2?"},
{0x0040,"Tarifregister 2"},
{0x0041,"Tarifregister 3"},
{0x0044,"Volumenregister V1"},
{0x0045,"Volumenregister V2"},
{0x0054,"Inputregister VA"},
{0x0055,"Inputregister VB"},
{0x0048,"Masseregister V1"},
{0x0049,"Masseregister V2"},
{0x03EC,"Drifttimet�ller"},
{0x0071,"Info-eventt�ller"},
{0x03EA,"Aktuelt klokkesl�t (hhmmss)"},
{0x0063,"Infokode register, aktuelt"},
{0x0056,"Aktuel freml�bstemperatur"},
{0x0057,"Aktuel returl�bstemperatur"},
{0x0058,"Aktuel temperatur T3"},
{0x007A,"Aktuel temperatur T4"},
{0x0059,"Aktuel temperaturdifferens"},
{0x005B,"Tryk i freml�b"},
{0x005C,"Tryk i returl�b"},
{0x004A,"Aktuelt flow i freml�b"},
{0x004B,"Aktuelt flow i returl�b"},
{0x0050,"Aktuel effekt beregnet p� baggrund af V1-T1-T2."},
{0x007B,"Dato for max. i indev�rende �r"},
{0x007C,"Max. v�rdi i indev�rende �r"},
{0x007D,"Dato for min. i indev�rende �r"},
{0x007E,"Min. v�rdi i indev�rende �r"},
{0x007F,"Dato for max. i indev�rende �r"},
{0x0080,"Max. v�rdi i indev�rende �r"},
{0x0081,"Dato for min. i indev�rende �r"},
{0x0082,"Min. v�rdi i indev�rende �r"},
{0x008A,"Dato for max. i indev�rende m�ned"},
{0x008B,"Max. v�rdi i indev�rende m�ned"},
{0x008C,"Dato for min. i indev�rende m�ned"},
{0x008D,"Min. v�rdi i indev�rende m�ned"},
{0x008E,"Dato for max. i indev�rende m�ned"},
{0x008F,"Max. v�rdi i indev�rende m�ned"},
{0x0090,"Dato for min. i indev�rende m�ned"},
{0x0091,"Min. v�rdi i indev�rende m�ned"},
{0x0092,"�r til dato gennemsnit for T1"},
{0x0093,"�r til dato gennemsnit for T2"},
{0x0095,"M�ned til dato gennemsnit for T1"},
{0x0096,"M�ned til dato gennemsnit for T2"},
{0x0042,"Tarifgr�nse 2"},
{0x0043,"Tarifgr�nse 3"},
{0x0062,"Sk�ringsdato (afl�sningsdato)"},
{0x0098,"Program nr. ABCCCCCC"},
{0x0099,"Config nr. DDDEE"},
{0x00A8,"Config. nr. FFGGMN"},
{0x03E9,"Serienr. (unikt nummer for hver m�ler)"},
{0x0070,"Kundenummer (8 mest betydende cifre)"},
{0x03F2,"Kundenummer (8 mindst betydende cifre)"},
{0x0072,"M�lernr. for VA"},
{0x0068,"M�lernr. for VB"},
{0x03ED,"Software-edition"},
{0x009A,"Software-checksum"},
{0x009B,"H�jopl�seligt energiregister til testform�l"},
{0x009D,"ID-nummer for topmodul"},
{0x009E,"ID-nummer for bundmodul"},
{0x00AF,"Fejltimet�ller"},
{0x00EA,"l/imp. for VA"},
{0x00EB,"l/imp. for VB"}
};

const uint16_t commands[K602_CMD_COUNT] =
{
0x03EB,0x003C,0x005E,0x003F,0x003D,0x003E,0x005F,0x0060,0x0061,0x006E,
0x0040,0x0041,0x0044,0x0045,0x0054,0x0055,0x0048,0x0049,0x03EC,0x0071,
0x03EA,0x0063,0x0056,0x0057,0x0058,0x007A,0x0059,0x005B,0x005C,0x004A,
0x004B,0x0050,0x007B,0x007C,0x007D,0x007E,0x007F,0x0080,0x0081,0x0082,
0x008A,0x008B,0x008C,0x008D,0x008E,0x008F,0x0090,0x0091,0x0092,0x0093,
0x0095,0x0096,0x0042,0x0043,0x0062,0x0098,0x0099,0x00A8,0x03E9,0x0070,
0x03F2,0x0072,0x0068,0x03ED,0x009A,0x009B,0x009D,0x009E,0x00AF,0x00EA,
0x00EB
};
*/

//#else

/* KAMSTRUP 382 commands

# 0x0001: "Energy in",
# 0x0002: "Energy out",
#   0x0003: "Reactive energy import",
#   0x0004: "Reactive energy export",
# 0x000d: "Energy in hi-res",
# 0x000e: "Energy out hi-res",
# 0x041e: "Voltage p1",
# 0x041f: "Voltage p2",
# 0x0420: "Voltage p3",
# 0x0434: "Current p1",
# 0x0435: "Current p2",
# 0x0436: "Current p3",
# 0x0438: "Power p1",
# 0x0439: "Power p2",
# 0x043a: "Power p3",

#   0x03FF: "1023 Total active power import",
#   0x0400: "1024 Total active power export",
#   0x0401: "1025 Total reactive power import",
#   0x0402: "1026 Total reactive power export"

1,"Active energy A14",     //+
2,"Active energy A23",     //+
1031,"Active energy A1234",    // samme som 0x0001
3,"Reactive energy R12",    //+
4,"Reactive energy R34",    //+
5,"Reactive energy R1",
6,"Reactive energy R4",
13,"Active energy A14, test xxx.xxxx", //- 0.0
14,"Active energy A23, test xxx.xxxx", //- 0.0
x000E,"Reactive energy R12, test xxx.xxxx", //- 0.0
16,"Reactive energy R34, test xxx.xxxx",
19,"Active energy A14 Tariff 1",   //- samme som 0x0001
23,"Active energy A14 Tariff 2",
27,"Active energy A14 Tariff 3",
31,"Active energy A14 Tariff 4",
1059,"Active energy A14 Tariff 5",
1060,"Active energy A14 Tariff 6",
1061,"Active energy A14 Tariff 7",
1062,"Active energy A14 Tariff 8",
20,"Active energy A23 Tariff 1",   //- 0.0
24,"Active energy A23 Tariff 2",
28,"Active energy A23 Tariff 3",
32,"Active energy A23 Tariff 4",
1063,"Active energy A23 Tariff 5",
1064,"Active energy A23 Tariff 6",
1065,"Active energy A23 Tariff 7",
1066,"Active energy A23 Tariff 8",
21,"Reactive energy R12 Tariff 1",
25,"Reactive energy R12 Tariff 2",
29,"Reactive energy R12 Tariff 3",
33,"Reactive energy R12 Tariff 4",
1067,"Reactive energy R12 Tariff 5",
1068,"Reactive energy R12 Tariff 6",
1069,"Reactive energy R12 Tariff 7",
1070,"Reactive energy R12 Tariff 8",
22,"Reactive energy R34 Tariff 1",
26,"Reactive energy R34 Tariff 2",
30,"Reactive energy R34 Tariff 3",
34,"Reactive energy R34 Tariff 4",
1071,"Reactive energy R34 Tariff 5",
1072,"Reactive energy R34 Tariff 6",
1073,"Reactive energy R34 Tariff 7",
1074,"Reactive energy R34 Tariff 8",
17,"Resetable counter A14",
18,"Resetable counter A23",
39,"Max power P14",
40,"Max power P23",
41,"Max power Q12",
42,"Max power Q34",
1023,"Actual power P14",
1024,"Actual power P23",
1025,"Actual power Q12",
1026,"Actual power Q34",
43,"Accumulated max power P14",
44,"Accumulated max power P23",
45,"Accumulated max power Q12",
46,"Accumulated max power Q34",
47,"Number of debiting periods",
1049,"Max power P14 RTC",
58,"Pulse input",
1004,"Hour counter",
1002,"Clock",
1003,"Date",
1047,"RTC",
54,"Configurations number 1",
55,"Configurations number 2",
56,"Configurations number 3",
1029,"Configurations number 4",
1075,"Configurations number 5",
57,"Special Data 1",
1021,"Special Data 2",
1010,"Total meter number",
51,"Meter number 1",
52,"Meter number 2",
53,"Meter number 3",
50,"Meter status
1001,"Serial number
1058,"Type number
2010,"Active tariff
1032,"Operation mode
1033,"Max power P14 Tariff 1
1050,"Max power P14 Tariff 1 RTC
1036,"Max power P14 Tariff 2
1051,"Max power P14 Tariff 2 RTC
1039,"Power threshold value
1040,"Power threshold counter
1045,"RTC status
1046,"VCOPCO status
1054,"Voltage L1",      //+
1055,"Voltage L2",      //+
1056,"Voltage L3",      //+
1076,"Current L1",      //+
1077,"Current L2",      //+
1078,"Current L3",      //+
1080,"Actual power P14 L1",   //+
1081,"Actual power P14 L2",   //+
1082,"Actual power P14 L3",   //+
1083,"ROM checksum",
1005,"Software revision",
1084,"Voltage extremity",
1085,"Voltage event",
1086,"Logger status",
1087,"Connection status",
1088,"Connection feedback",
1102,"Module port I/O configuration",

*/

typedef struct
{
	short cmd;
	char *txt;
} cmdtxt_t;

char *cmdtxt(cmdtxt_t *table, uint8_t cnt, uint16_t c);

#endif